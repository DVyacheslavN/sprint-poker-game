/*
   Sprint Poker Game - A virtual poker game for agile development teams.
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package http_server

import (
	"crypto/tls"
	"fmt"
	"net/http"

	socketio "github.com/googollee/go-socket.io"
)

type HandlerFunc func([]string) string

type Pair struct {
	function  HandlerFunc
	variables []string
}

// var mux = http.NewServeMux()
var mux = NewDynamicMux()
var handlers = make(map[string]Pair)

var tlsConfig = &tls.Config{
	MinVersion:               tls.VersionTLS12,
	PreferServerCipherSuites: true,
}

var server = &http.Server{
	Addr:    ":3329",
	Handler: mux,
}

func handler(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()
	path := r.URL.Path
	retVal := ""
	if val, ok := handlers[path]; ok {
		args := []string{}
		for _, value := range val.variables {
			getVal := values.Get(value)
			args = append(args, getVal)
		}
		retVal = val.function(args)
	} else {
		fmt.Println("now such url:", path)
	}
	fmt.Fprint(w, retVal)
}

func RegisterHandler(url string, function HandlerFunc, args []string) {
	mux.RegisterHandler(url, function, args)
}

func SetPublicFolder(url string, path string) func(bool) {

	return mux.SetPublicFolder(url, path, 0)
}

func SetPublicFile(url string, path string) {

	mux.SetPublicFile(url, path)
}

func HandleSocketIo(url string, socket *socketio.Server) {
	mux.RegisterSocket(url, socket)
	go socket.Serve()
}

func Listen() {
	// mux.HandleFunc("/H/", handler)
	go mux.RemoveInactiveHandlers()
	fmt.Println("Server listening on port 3329...")
	if err := server.ListenAndServe(); err != nil {
		fmt.Println("Error starting server:", err)
	}
}
