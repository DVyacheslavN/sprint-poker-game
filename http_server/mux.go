/*
   Sprint Poker Game - A virtual poker game for agile development teams.
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package http_server

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	socketio "github.com/googollee/go-socket.io"
)

type DynamicMux struct {
	handlers   map[string]http.Handler
	active     map[string]bool
	lastAccess map[string]time.Time
	mu         sync.RWMutex
}

func NewDynamicMux() *DynamicMux {
	return &DynamicMux{
		handlers:   make(map[string]http.Handler),
		active:     make(map[string]bool),
		mu:         sync.RWMutex{},
		lastAccess: make(map[string]time.Time),
	}
}

func (dm *DynamicMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	dm.mu.RLock()
	handler, active := dm.handlers[r.URL.Path], dm.active[r.URL.Path]
	dm.mu.RUnlock()

	if !active || handler == nil {
		http.NotFound(w, r)
		return
	}

	dm.lastAccess[r.URL.Path] = time.Now()
	handler.ServeHTTP(w, r)
}

func CustomFileServer(path string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		path1 := "./" + path
		log.Println("LOG path:", path1)
		http.ServeFile(w, r, path1)
	})
}

func depth(basePath, path string) int {
	rel, err := filepath.Rel(basePath, path)
	if err != nil {
		return -1 // Error case
	}
	return strings.Count(rel, string(filepath.Separator))
}

func (dm *DynamicMux) SetPublicFolder(url string, path string, maxDepth int) func(bool) {
	dm.mu.Lock()
	defer dm.mu.Unlock()
	sessionRelatedPaths := []string{}
	err := filepath.Walk(path, func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		currentDepth := depth(path, filePath)
		if currentDepth < 0 {
			return nil
		}

		if currentDepth > maxDepth {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		if !info.IsDir() {
			relativePath, err := filepath.Rel(path, filePath)
			if err != nil {
				return err
			}
			urlPath := filepath.ToSlash(relativePath)
			filename := filepath.Base(filePath)
			if filename == "index.html" {
				urlPath = ""
			}
			dm.handlers[url+urlPath] = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				http.ServeFile(w, r, filePath)
			})
			dm.active[url+urlPath] = true
			dm.lastAccess[url+urlPath] = time.Now()
			sessionRelatedPaths = append(sessionRelatedPaths, url+urlPath)
		}
		return nil
	})
	if err != nil {
		log.Printf("Error walking through directory: %v", err)
	}

	// fix of socket io iusse
	sessionRelatedPaths = append(sessionRelatedPaths, url+"1/")
	log.Printf("Registered %s", url)

	return func(active bool) {
		dm.mu.Lock()
		defer dm.mu.Unlock()
		log.Printf("Set active=%t for %s\n", active, sessionRelatedPaths[0])
		for _, path := range sessionRelatedPaths {
			dm.active[path] = active
			// log.Printf("Set active=%t for %s\n", active, path)
		}
	}
}

func (dm *DynamicMux) SetPublicFile(url string, path string) {
	dm.mu.Lock()
	defer dm.mu.Unlock()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, path)
	})
	dm.handlers[url] = handler
	dm.active[url] = true
}

func (dm *DynamicMux) RemoveHandler(url string) {
	dm.mu.Lock()
	defer dm.mu.Unlock()
	delete(dm.handlers, url)
	delete(dm.active, url)
}

func (dm *DynamicMux) RegisterHandler(url string, function HandlerFunc, fields []string) {
	dm.mu.Lock()
	defer dm.mu.Unlock()
	dm.handlers[url] = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		values := r.URL.Query()
		retVal := ""
		args := []string{}
		for _, value := range fields {
			getVal := values.Get(value)
			args = append(args, getVal)
		}
		retVal = function(args)
		fmt.Fprint(w, retVal)

	})
	dm.active[url] = true
}

func (dm *DynamicMux) RemoveInactiveHandlers() {
	ticker := time.NewTicker(20 * time.Minute)
	for {
		select {
		case <-ticker.C:
			dm.mu.Lock()
			now := time.Now()
			for path, lastAccess := range dm.lastAccess {
				if now.Sub(lastAccess) > 20*time.Minute && !dm.active[path] {
					delete(dm.handlers, path)
					delete(dm.active, path)
					delete(dm.lastAccess, path)
				}
			}
			log.Println("Paths left, active:", len(dm.active), "     links:", len(dm.handlers))
			dm.mu.Unlock()
		}
	}
}

func (dm *DynamicMux) RegisterSocket(url string, socket *socketio.Server) {
	dm.mu.Lock()
	defer dm.mu.Unlock()
	dm.handlers[url] = socket
	dm.active[url] = true
}
