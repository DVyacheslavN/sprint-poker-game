/*
   Sprint Poker Game - A virtual poker game for agile development teams.
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"log"
	"poker/http_server"
	"poker/session"
	"poker/socket"

	socketio "github.com/googollee/go-socket.io"
)

func randomLink() string {
	bytes := 32
	rBytes := make([]byte, bytes)
	_, err := rand.Read(rBytes)
	if err != nil {
		fmt.Println("Error:", err)
		return ""
	}
	return "/" + base64.StdEncoding.EncodeToString(rBytes) + "/"
}

func sendVoices(sessoin *session.SessionData, socket *socketio.Server) {
	msg := make(map[string]any)
	for _, VoiceInfo := range sessoin.GetAllVoices() {
		userId := VoiceInfo.Id
		msg[userId] = map[string]any{
			"USERID": VoiceInfo.Id,
			"vote":   VoiceInfo.Voice,
		}
	}
	socket.BroadcastToNamespace("/", "user_voices", msg)
}

func usersUpdated(sessoin *session.SessionData, socket *socketio.Server) {
	msg := make(map[string]any)
	for _, UserInfo := range sessoin.GetAllUsers() {
		userId := UserInfo.Id
		msg[userId] = map[string]string{
			"id":   userId,
			"name": UserInfo.Name,
		}
	}
	socket.BroadcastToNamespace("/", "users_added", msg)
}

func bindSession(sessoin *session.SessionData, socket *socketio.Server, triggre_active func(bool)) {

	socket.OnEvent("", "username", func(s socketio.Conn, user map[string]string) {
		sessoin.AddUser(user["userId"], user["userName"], s.ID())
		usersUpdated(sessoin, socket)
		sendVoices(sessoin, socket)
	})

	socket.OnEvent("", "reset_votes", func(s socketio.Conn, _ any) {
		if !sessoin.IsUserRegisted(s.ID()) {
			return
		}

		sessoin.ResetAllVoices()
		sendVoices(sessoin, socket)
	})

	socket.OnEvent("", "reveal_all", func(s socketio.Conn, status any) {
		if !sessoin.IsUserRegisted(s.ID()) {
			return
		}
		socket.BroadcastToNamespace("/", "reveal_all_s", status)
	})

	socket.OnEvent("", "user_vote", func(s socketio.Conn, voute map[string]string) {
		if !sessoin.IsUserRegisted(s.ID()) {
			return
		}
		sessoin.SetVoice(s.ID(), voute["vote"])
		sendVoices(sessoin, socket)
	})

	socket.OnConnect("/", func(s socketio.Conn) error {
		log.Println("Connected:", s.ID())
		triggre_active(true)
		return nil
	})
	socket.OnDisconnect("/", func(s socketio.Conn, reason string) {
		sessoin.RemoveUser(s.ID())
		if len(sessoin.GetAllUsers()) == 0 {
			triggre_active(false)
		}

		usersUpdated(sessoin, socket)
		log.Println("Disconnected:", s.ID())
	})

}

func create(values []string) string {
	link := randomLink()
	triggre_active := http_server.SetPublicFolder(link, "public/")
	socket_ := socket.GetSocketIo("/")
	session := session.NewSessionData(link)
	bindSession(session, socket_, triggre_active)
	http_server.HandleSocketIo(link+"1/", socket_)
	return link
}

func main() {
	http_server.RegisterHandler("/H/create", create, []string{})
	http_server.SetPublicFile("/", "public/create/")
	http_server.Listen()

}
