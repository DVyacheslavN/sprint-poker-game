/*
   Sprint Poker Game - A virtual poker game for agile development teams.
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package session

import (
	"log"
	"sync"
)

type UserFields struct {
	name  string
	voice string
	user  string
}

type UserInfo struct {
	Id   string
	Name string
}

type VoiceInfo struct {
	Id    string
	Voice string
}

type SessionData struct {
	Id       string
	userData map[string]*UserFields
	mu       sync.Mutex
}

var user_counter = 0

func NewSessionData(id_ string) *SessionData {
	return &SessionData{
		Id:       id_,
		userData: make(map[string]*UserFields),
	}
}

func (s *SessionData) AddUser(user_ string, name_ string, socketid_ string) {
	if _, exists := s.userData[socketid_]; !exists {
		user_counter++
		log.Println("USERS :", user_counter)
	}

	s.userData[socketid_] = &UserFields{
		name:  name_,
		voice: "",
		user:  user_,
	}
}

func (s *SessionData) IsUserRegisted(socketid_ string) bool {
	if _, exists := s.userData[socketid_]; exists {
		return true
	}
	return false
}

func (s *SessionData) RemoveUser(socketid_ string) {
	delete(s.userData, socketid_)
}

func (s *SessionData) SetVoice(socketid_ string, voice string) {
	if userFields, exists := s.userData[socketid_]; exists {
		userFields.voice = voice
	}
}

func (s *SessionData) ResetAllVoices() {
	s.mu.Lock()
	defer s.mu.Unlock()

	for key := range s.userData {
		user := s.userData[key]
		user.voice = ""
		s.userData[key] = user
	}
}

func (s *SessionData) GetAllVoices() []VoiceInfo {
	voiceInfo := make([]VoiceInfo, 0, len(s.userData))
	for key := range s.userData {
		voiceInfo = append(voiceInfo, VoiceInfo{Id: s.userData[key].user, Voice: s.userData[key].voice})
	}
	return voiceInfo
}

func (s *SessionData) GetAllUsers() []UserInfo {
	userInfo := make([]UserInfo, 0, len(s.userData))
	for key := range s.userData {
		userInfo = append(userInfo, UserInfo{Id: s.userData[key].user, Name: s.userData[key].name})
	}
	return userInfo
}
