/*
   Sprint Poker Game - A virtual poker game for agile development teams.
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

function generateUserId(length = 10) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const USERID = generateUserId();



let USERNAME = ""
document.addEventListener('DOMContentLoaded', () => {
    const usernameDisplay = document.getElementById('usernameDisplay');
    const usernameInput = document.getElementById('usernameInput');
    const editUsernameBtn = document.getElementById('editUsernameBtn');
    const saveUsernameBtn = document.getElementById('saveUsernameBtn');

    // Function to toggle edit mode
    function toggleEdit(save = false) {

        if (save) {
            console.log("SAVE:")
            socket.emit('username', { userId: USERID, userName: usernameInput.value }); // Send the vote to the server
            USERNAME = usernameInput.value
            // Save the username from the input to the display span and socket
            usernameDisplay.textContent = usernameInput.value;
            usernameInput.style.display = 'none';
            usernameDisplay.style.display = 'inline';
            saveUsernameBtn.style.display = 'none';
            editUsernameBtn.style.display = 'inline';
            // Here, you could also emit the new username to the server via socket
            console.log(`Username changed to: ${usernameInput.value}`);
        } else {
            // Switch to edit mode
            usernameInput.style.display = 'inline-block';
            usernameDisplay.style.display = 'none';
            saveUsernameBtn.style.display = 'inline';
            editUsernameBtn.style.display = 'none';
            usernameInput.value = "" // Pre-fill input with current username
            usernameInput.focus(); // Focus the input field for immediate editing
        }
    }

    // Initially, set the username from a predefined or stored value
    usernameDisplay.textContent = 'Default Username'; // Placeholder, replace with actual username
    usernameInput.style.display = 'none';
    usernameDisplay.style.display = 'inline';

    editUsernameBtn.addEventListener('click', () => toggleEdit(false));
    saveUsernameBtn.addEventListener('click', () => toggleEdit(true));

    // Optional: Save username on enter key
    usernameInput.addEventListener('keypress', (event) => {
        if (event.key === 'Enter') {
            toggleEdit(true);
        }
    });
});

let mUsers = {};
let newUsers;
let mVotes = {};
let votesRevealed = false; 


function changeBtReveal(value, revealButton) {
    votesRevealed = value
    document.querySelectorAll('.card').forEach(card => {
        if (votesRevealed) {
            card.classList.add('flipped');
        } else {
            card.classList.remove('flipped');
        }
    });
    if (!votesRevealed) {
        socket.emit('reset_votes')
        const voteButtons = document.querySelectorAll('.vote-button');
        voteButtons.forEach(button => {
            button.classList.remove('card-selected');
        });
    }
    // Update the button text based on the state
    revealButton.textContent = votesRevealed ? 'Hide Votes' : 'Reveal Votes';

    // Toggle visibility of votes based on 'votesRevealed'
    if (votesRevealed) {
        const votes_ = Object.values(mVotes).map(vote => vote.vote);
        let iterator = 0;
        let sum = 0;
        votes_.forEach(vote => {
            if (/^-?\d+$/.test(vote) && vote > 0) {
                iterator++;
                sum += parseInt(vote, 10);
                console.log("AVR:", "SUM:", sum, "iterantor:", iterator)
            }
        })
        let average = iterator > 0 ? (sum / iterator).toFixed(1) : '--';
        document.getElementById('averageVote').textContent = `Avg: ${average}`;
    } else {
        mVotes = {};
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const container = document.querySelector('.vote-cards-container');
    const revealButton = document.getElementById('revealVotes');
// Tracks the state of vote revelation

    // Assume 'socket' is already connected
    socket.on('users_added', (users) => {
        console.log("Update votes received:", users);
        // Assuming 'data' includes 'userId' and 'vote'
        newUsers = users;
        // Check if the user hasn't been added before or if you need to update their vote
        Object.keys(users).forEach(key => {
            updateVoteDisplay(users[key].id, users[key].name);
        })

        if (Object.keys(mUsers).length > Object.keys(users).length) {
            const usersKeys = Object.values(users).map(user => user.id);
            const mUsersKeys = Object.keys(mUsers);
            const missingKeys = mUsersKeys.filter(key => !usersKeys.includes(key))
            console.log("mUsersKeys DDDD:", mUsersKeys)
            console.log("usersKeys DDDD:", usersKeys)
            console.log("missingKeys DDDD:", missingKeys)
            missingKeys.forEach(key => {
                card = document.getElementById(key + 'elm');
                delete mUsers[key];
                if (card)
                    card.remove()
            });
        }
        // Pass the entire users object or just the necessary part

    });

    socket.on('user_removed', (userId) => {
        console.log("userRemoved", userId);
        card = document.getElementById(userId + 'elm');
        delete mUsers[userId];
        if (card)
            card.remove()
    });

    socket.on('reveal_all_s', (status) => {
        console.log("reveal_all_s", status);
        changeBtReveal(status, revealButton);
    });

    revealButton.addEventListener('click', () => {
        changeBtReveal(!votesRevealed, revealButton);
            socket.emit('reveal_all', votesRevealed)
    });

    function updateVoteDisplay(userId, username) {
        console.log("Current users:", mUsers)
        if (!mUsers[userId]) {
            mUsers[userId] = username; // Add or update the user's vote
            const container = document.querySelector('.vote-cards-container');

            // Iterate through votes and create a card for each
            const card = createVoteCard(username, userId);
            container.appendChild(card);
        }
        front_card = document.getElementById(userId + 'front');
        if (front_card)
            front_card.textContent = username
    }

    function createVoteCard(username, userId) {
        // Create elements for the card here, similar to before, but use 'clientId' and 'vote'
        const cardElement = document.createElement('div');
        cardElement.className = 'card';
        cardElement.id = userId + 'elm';
        const cardInner = document.createElement('div');
        cardInner.className = 'card-inner';
        const cardFront = document.createElement('div');
        cardFront.className = 'card-front';
        cardFront.textContent = username; // Show clientId on the card front
        cardFront.id = userId + 'front';
        const cardBack = document.createElement('div');
        cardBack.className = 'card-back';
        cardBack.textContent = '--'; // Show vote on the card back
        cardBack.id = userId + 'back';

        cardInner.appendChild(cardFront);
        cardInner.appendChild(cardBack);
        cardElement.appendChild(cardInner);
        // Add flip logic or any other interactive features as needed

        return cardElement;
    }
});


document.addEventListener('DOMContentLoaded', function () {
    const voteButtons = document.querySelectorAll('.vote-button');
    voteButtons.forEach(button => {
        button.addEventListener('click', function () {
            // Remove selected class from all buttons
            voteButtons.forEach(btn => btn.classList.remove('card-selected', 'card-highlighted'));
            const vote = this.textContent;
            socket.emit('user_vote', { vote, USERID }); // Send the vote to the server
            // Add selected class to the clicked button
            this.classList.add('card-selected');

            // Optionally, you could highlight this button further or take additional actions
        });
    });

    socket.on('user_voices', (voices) => {
        console.log('Vote received from server:', voices);
        mVotes = voices;
        Object.keys(voices).forEach(key => {
            back_card = document.getElementById(voices[key].USERID + 'back');
            front_card = document.getElementById(voices[key].USERID + 'front');
            if (back_card) {
                console.log("SET vote:", voices[key].vote)
                if (voices[key].vote == 0 || voices[key].vote == '--')
                    back_card.textContent = front_card.textContent + '\n' + '--'
                else
                    back_card.textContent = front_card.textContent + '\n' + voices[key].vote;
            }

            if (front_card) {
                if (voices[key].vote != 0 && voices[key].vote != '--')
                    front_card.classList.add('card-selected');
                else
                    front_card.classList.remove('card-selected');

            }
        })
    });
});

document.addEventListener('DOMContentLoaded', () => {
    // Assuming each card has a class 'card'
    const cards = document.querySelectorAll('.card');

    cards.forEach(card => {
        card.addEventListener('click', () => {
            // Toggle selected state on click
            card.classList.toggle('card-selected');

            // Optionally, add logic here to highlight the card in dark yellow under certain conditions
            // For demonstration, let's highlight it when it's clicked again while selected
            if (card.classList.contains('card-selected')) {
                card.classList.add('card-highlighted');
            } else {
                card.classList.remove('card-highlighted');
            }
        });
    });
});





