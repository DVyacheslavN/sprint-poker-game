# Sprint Poker Game

Sprint Poker Game is a digital tool designed to facilitate agile planning sessions. It enables teams to estimate tasks efficiently through a fun, interactive poker game format. This tool aims to streamline the planning process and enhance team collaboration.

## Features

- **Real-time Voting**: Team members can vote on task complexities in real-time.
- **Anonymous Voting**: Ensures that votes are anonymous to prevent bias in task estimation.
- **Automatic Calculation**: Automatically calculates and displays average scores for tasks to speed up the decision-making process.
- **Mobile Friendly**: Accessible on various devices, supporting both desktop and mobile use.

## Getting started

These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What you need to install the software:
    golang 1.20

### Installation

 git clone   
 install golang
cd sprint-poker-game
go run main.go


## Usage

To start a new planning session, navigate to the main page and create a session. Invite team members by sharing the session link.

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are greatly appreciated.

## Authors

Viacheslav Domnin - Initial work - vyacheslavdomnin@sdevltd.onmicrosoft.com

## License

This project is licensed under the GPL v2 License - see the [LICENSE](LICENSE) file for details.